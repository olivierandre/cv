<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    /**
     * @Route("/api/user/getcurrentuser", name="get_current_user")
     */
    public function getCurrentUser()
    {
        return $this->json([
          'user' => $this->getUser()
        ]);
    }
}
