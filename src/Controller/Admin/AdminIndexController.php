<?php

namespace App\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Group;
use App\Form\GroupType;

class AdminIndexController extends Controller
{
    /**
     * @Route("/admin", name="admin_index")
     */
    public function index(Request $request)
    {
        return $this->render('admin/admin_index/index.html.twig', [
            'adminMenuCurriculumVitae' => $this->generateUrl('admin_curriculum_vitae_index'),
            'adminMenuEducation' => $this->generateUrl('admin_education_index'),
            'adminMenuWork' => $this->generateUrl('admin_work_index')
        ]);
    }
}
