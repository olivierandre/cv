<?php

namespace App\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\CurriculumVitae;
use App\Form\CurriculumVitaeType;
use App\Service\FormService;

class AdminCurriculumVitaeController extends Controller
{
    /**
     * @Route("/admin/curriculum/vitae",
     * name="admin_curriculum_vitae_index",
     * methods={"GET","HEAD","POST","PUT"}
     * )
     */
    public function index(Request $request, FormService $formService)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $method = 'PUT';
        $curriculumVitae = $entityManager->getRepository(CurriculumVitae::class)->findOneByUser($user);

        if (!$curriculumVitae) {
            $curriculumVitae = new CurriculumVitae();
            $method = 'POST';
        }

        $form = $this->createForm(CurriculumVitaeType::class, $curriculumVitae, ['method' => $method]);
        $form->handleRequest($request);

        $fields = $formService->getForm($form, $curriculumVitae);

        if ($form->isSubmitted() && $form->isValid()) {
            $curriculumVitae = $form->getData();
            $curriculumVitae->setUser($user);

            $entityManager->persist($curriculumVitae);
            $entityManager->flush();
        } else if ($form->isSubmitted() && !$form->isValid()) {
          // TODO: Errors treatement
        }

        return $this->json($fields);
    }
}
