<?php

namespace App\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Work;
use App\Entity\CurriculumVitae;
use App\Form\WorkType;
use App\Service\FormService;

class AdminWorkController extends Controller
{
    /**
     * @Route("/admin/work", name="admin_work_index")
     */
    public function index(Request $request, FormService $formService)
    {
        $work = new Work();

        $form = $this->createForm(WorkType::class, $work);
        $form->handleRequest($request);
        $fields = $formService->getForm($form, $work);

        if ($form->isSubmitted() && $form->isValid()) {
            $work = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $user = $this->getUser();
            $curriculumVitae = $entityManager->getRepository(CurriculumVitae::class)->findOneByUser($user);
            $work->setCurriculumVitae($curriculumVitae);

            //$fields['image'] = $request->files;


            $entityManager->persist($work);
            $entityManager->flush();

            //dump();die;
        }

        // return $this->render('admin/admin_work/index.html.twig', [
        //     'form' => $form->createView()
        // ]);

        return $this->json($fields);
    }
}
