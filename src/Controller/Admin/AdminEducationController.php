<?php

namespace App\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Education;
use App\Entity\CurriculumVitae;
use App\Form\EducationType;
use App\Service\FormService;

class AdminEducationController extends Controller
{
    /**
     * @Route("/admin/education/{id}", name="admin_education_index", methods={"GET","HEAD","POST","PUT"})
     */
    public function index($id = false, Request $request, FormService $formService)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $education = new Education();

        $form = $this->createForm(EducationType::class, $education);
        $form->handleRequest($request);

        $fields = $formService->getForm($form, $education, true);

        if ($form->isSubmitted() && $form->isValid()) {
            $education = $form->getData();
            $curriculumVitae = $entityManager->getRepository(CurriculumVitae::class)->findOneByUser($user);
            $education->setCurriculumVitae($curriculumVitae);

            $entityManager->persist($education);
            $entityManager->flush();
        }

        return $this->json($fields);
    }
}
