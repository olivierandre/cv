<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use App\Entity\User;
use App\Form\LoginType;

class SecurityController extends Controller
{
    /**
       * @Route("/login", name="login")
       */
    public function login(Request $request, AuthenticationUtils $authUtils)
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_index');
        }
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();
        if($error) {
          $this->addFlash("danger", $error->getMessageKey());
        }

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        $user = new User();
        if($lastUsername) {
          $user->setUsername($lastUsername);
        }
        $form = $this->createForm(LoginType::class, $user);

        return $this->render('security/login.html.twig', array(
          'last_username' => $lastUsername,
          'title' => 'Formulaire de connexion',
          'form' => $form->createView()
        ));
    }
}
