<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Entity\User;

class IndexController extends Controller
{
    /**
     * @Route("/", name="app_index"),
     */
    public function index()
    {
        $number = mt_rand(0, 100);

        return $this->render('index.html.twig', [
          'title' => 'lucky number',
          'number' => $number,
        ]);
    }
}
