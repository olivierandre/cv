<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="groups")
 * @ORM\Entity(repositoryClass="App\Repository\GroupRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("name", message="Ce nom de rôle existe déjà")
 * @UniqueEntity("role", message="Ce rôle existe déjà")
 */
class Group extends Role
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string Name
     *
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message="Veuillez fournir un nom pour le rôle")
     * @Assert\Length(
     *          max="30",
     *          maxMessage="le nom du rôle ne peut pas être plus long que {{ limit }} caractères"
     * )
     */
    private $name;

    /**
     * @ORM\Column(name="role", type="string", length=20, unique=true)
     * @Assert\NotBlank(message="Veuillez fournir un rôle")
     * @Assert\Length(
     *          max="20",
     *          maxMessage="le rôle ne peut pas être plus long que {{ limit }} caractères"
     * )
     * @Assert\Regex(
     *     pattern="/ROLE_[A-Z]{3,}/",
     *     message="Le nom du rôle doit toujours commencer par 'ROLE_', ne comporter que des majuscules"
     * )
     */
    private $role;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="roles")
     */
    private $users;

    /**
     * @ORM\Column(name="date_created", type="datetime")
     * @Assert\DateTime()
     */
    private $dateCreated;

    /**
     * @ORM\Column(name="date_modified", type="datetime")
     * @Assert\DateTime()
     */
    private $dateModified;

    public function __construct()
    {
        //$this->users = new ArrayCollection();
    }

    /**
     * @see RoleInterface
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Group
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Add users
     *
     * @param \App\Entity\User $users
     * @return Group
     */
    public function addUser(\App\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \App\Entity\User $users
     */
    public function removeUser(\App\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return Group
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     * @return Group
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;

        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @ORM\PrePersist
     */
    public function beforeInsert()
    {
        $this->setDateCreated(new \DateTime());
        $this->setDateModified(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function beforeEdit()
    {
        $this->setDateModified(new \DateTime());
    }
}
