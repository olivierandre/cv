<?php

namespace App\Service;

class ToolsService
{
    public function getMethodFromClass($class, string $search_text)
    {
        $methods = get_class_methods($class);


        $value = array_filter($methods, function ($el) use ($search_text) {
            return strpos(strtolower($el), strtolower($search_text)) !== false ;
        });
        $function = reset($value);

        return $function;
    }
}
