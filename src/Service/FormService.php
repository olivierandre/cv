<?php

namespace App\Service;

use App\Service\ToolsService;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class FormService
{
    private $tools;
    private $csrfTokenManager;

    public function __construct(ToolsService $tools, CsrfTokenManagerInterface $csrfTokenManager)
    {
        $this->tools = $tools;
        $this->csrfTokenManager = $csrfTokenManager;
    }
    public function getForm(\Symfony\Component\Form\Form $form, $class, $clearValue = false)
    {
        $fields['form']['name'] = $form->getName();
        $fields['form']['method'] = $form->getConfig()->getMethod();
        $isSubmit = $form->isSubmitted();
        $hasFileInput = false;

        foreach ($form->all() as $field) {
            $name = $field->getName();
            $type = $field->getConfig()->getType()->getBlockPrefix();
            $function = $this->tools->getMethodFromClass($class, 'get'.$name);

            $value = '';
            if ($form->getData() && $type != "submit") {
                $value = $form->getData()->$function();
                if ($type == 'vich_image') {
                    //$value = !$isSubmit ? '' : $value->getClientOriginalName();
                    $value = null;
                    $hasFileInput = true;
                }
            }

            $errors = $field->getErrors()->current() ? $field->getErrors()->current()->getMessage() : '';

            $fields['form']['fields'][$name] = [
              'name' => $name,
              'type' => $type,
              'errors' => [
                'hasError' => !$isSubmit ? null : empty($errors),
                'message' => $errors
              ],
              'attributes' => !empty($field->getConfig()->getAttributes()['data_collector/passed_options']['attr']) ? $field->getConfig()->getAttributes()['data_collector/passed_options']['attr'] : [],
              'value' => $type != 'submit' && !$clearValue ? $value : '',
              'required' => $field->isRequired(),
            ];

            $fields['form']['fields'][$name]['attributes']['label'] = $field->getConfig()->getAttributes()['data_collector/passed_options']['label'];
        }
        $fields['form']['token'] = $this->csrfTokenManager->getToken($form->getName())->getValue();
        $fields['form']['hasFileInput'] = $hasFileInput;

        return $fields;
    }
}
