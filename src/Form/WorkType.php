<?php

namespace App\Form;

use App\Entity\Work;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class WorkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('projectName', TextType::class, [
              'label' => 'Nom du projet'
            ])
            ->add('description', TextareaType::class, [
              'label' => 'Description du projet'
            ])
            ->add('website', UrlType::class, [
              'label' => 'Adresse web du site'
            ])
            ->add('coverFile',VichImageType::class, [
              'label' => 'Couverture à uploader'
            ])
            ->add('save', SubmitType::class, [
              'label' => 'Create education'
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => Work::class,
        ]);
    }
}
