<?php

namespace App\Form;

use App\Entity\Education;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EducationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('period', TextType::class, [
              'label' => 'Période de formation'
            ])
            ->add('name', TextType::class, [
              'label' => 'Nom de la formation'
            ])
            ->add('place', TextType::class, [
              'label' => 'Lieu de la formation'
            ])
            ->add('diploma', TextType::class, [
              'label' => 'Nom du diplôme'
            ])
            ->add('save', SubmitType::class, [
              'label' => 'Create education'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Education::class,
        ]);
    }
}
