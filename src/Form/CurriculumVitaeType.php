<?php

namespace App\Form;

use App\Entity\CurriculumVitae;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CurriculumVitaeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
              'label' => 'Prénom',
              'attr' => [
                'placeholder' => 'Indiquer ici votre prénom',
                'description' => 'Ceci est une description'
              ]
            ])
            ->add('lastname', TextType::class, [
              'label' => 'Nom'
            ])
            ->add('email', EmailType::class, [
              'label' => 'Email'
            ])
            ->add('job', TextType::class, [
              'label' => 'Métier'
            ])
            ->add('phone', TextType::class, [
              'label' => 'Téléphone',
              'empty_data' => ''
            ])
            ->add('description', TextareaType::class, [
              'label' => 'Description'
            ])
            ->add('save', SubmitType::class, [
              'label' => 'Create Curriculum Vitae'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CurriculumVitae::class
        ]);
    }
}
