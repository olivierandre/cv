<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180311164544 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE work (id INT AUTO_INCREMENT NOT NULL, curriculumvitae_id INT DEFAULT NULL, project_name VARCHAR(120) NOT NULL, description LONGTEXT NOT NULL, website VARCHAR(120) NOT NULL, date_created DATETIME NOT NULL, date_modified DATETIME NOT NULL, cover_name VARCHAR(255) DEFAULT NULL, cover_original_name VARCHAR(255) DEFAULT NULL, cover_mime_type VARCHAR(255) DEFAULT NULL, cover_size INT DEFAULT NULL, cover_dimensions LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', INDEX IDX_534E688013818212 (curriculumvitae_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE work ADD CONSTRAINT FK_534E688013818212 FOREIGN KEY (curriculumvitae_id) REFERENCES curriculum_vitae (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE work');
    }
}
