<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Group;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $groupSuperAdmin = new Group();
        $groupSuperAdmin->setName('Super Admin');
        $groupSuperAdmin->setRole('ROLE_SUPER_ADMIN');
        $manager->persist($groupSuperAdmin);

        $groupAdmin = new Group();
        $groupAdmin->setName('Admin');
        $groupAdmin->setRole('ROLE_ADMIN');
        $manager->persist($groupAdmin);

        $groupUser = new Group();
        $groupUser->setName('User');
        $groupUser->setRole('ROLE_USER');
        $manager->persist($groupUser);

        $user = new User();
        $user->setFirstname('Olivier');
        $user->setLastname('André');
        $user->setUsername('olivier.andre77@gmail.com');
        $user->setEmail('olivier.andre77@gmail.com');
        $user->addRole($groupSuperAdmin);
        $password = getenv('ADMIN_PASSWORD');
        $encoded = $this->encoder->encodePassword($user, $password);
        $user->setPassword($encoded);
        $manager->persist($user);

        $manager->flush();
    }
}
