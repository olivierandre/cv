/* jshint esversion: 6 */

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import bContainer from 'bootstrap-vue/es/components/layout/container';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Alerte from './vue/Alert';
import Hello from './vue/Hello';
import Links from './vue/Links';

Vue.use(BootstrapVue, VueAxios, axios);

// new Vue({el: 'b-container', components: {
//     bContainer
//   }});
