/* jshint esversion: 6 */

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

import Hello from './vue/Hello';
import Links from './vue/Links';
import Alerte from './vue/Alert';
import bContainer from 'bootstrap-vue/es/components/layout/container';

Vue.use(BootstrapVue);

if (document.getElementById('admin_container')) {
  new Vue({
    el: '#admin_container',
    components: {
      Hello,
      Links,
      Alerte,
      bContainer
    }
  });
}
